# pcspeaker-music

Linux only

wait.sh usage: sudo ./wait.sh examples/X.txt

send.sh usage: sudo ./send.sh examples/X.txt IP1 IP2

# Starting

Run wait.sh on two PCs, providing different files

Run send.sh on third PC, providing the last file and the IPs of the other two PCs

# Troubleshooting

Make sure pcspkr kernel module is loaded, load with modprobe pcspkr if needed

Make sure PC speaker volume is loud enough, you may need to use alasmixer and/or enable speaker in BIOS

# Demo

https://youtu.be/VaHEh-kSFWI
