#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <linux/kd.h>

void siginthandler(int param) {
	ioctl(STDOUT_FILENO, KIOCSOUND, 0);
	exit(1);
}

int main(int argc, char *argv[]) {
	FILE *fp;
	char str[80];
	float freq, len;

	if (2 != argc) {
		printf("usage: %s <filename>\n", argv[0]);
		return 1;
	}

	fp = fopen(argv[1], "r");

	if (NULL == fp) {
		perror("fopen");
		return 2;
	}
	else {
		signal(SIGINT, siginthandler);

		while (!feof(fp)) {
			fscanf(fp, "%f %f %s", &freq, &len, &str);
			
			if (!feof(fp)) {
				if (freq > 0) {
					ioctl(STDOUT_FILENO, KIOCSOUND, (int) (1193180 / freq));
				}
				
				fprintf(stderr, "%f %f %s\n", freq, len, str);
				usleep(len * 1000000);
				ioctl(STDOUT_FILENO, KIOCSOUND, 0);
			}
		}

		fclose(fp);
	}

	return 0;
}
